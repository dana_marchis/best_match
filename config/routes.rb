Rails.application.routes.draw do
  get 'users/show'

  devise_for :users
  get 'welcome/index'
  resources :users
  root 'welcome#index'

end
